using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour
{
    
    public int enemyHealth;

    [Space]
    //  Префаб пули врага
    public GameObject obj_Bullet;
    //  Временной интервал для стрельбы
    public float shotTimeMin, shotTimeMax;
    //  Шанс стрельбы   
    public int shotChance;

    
    //  Depricated переехало в Wave
    // private void Start()
    // {
    //         // Вызов стрельбы (метод OpenFire) в промежутке между минимальным и максимальным
    //         //Invoke("OpenFire", Random.Range(shotTimeMin, shotTimeMax));
    // }
    // private void Update()
    // {
    //         // Вызов стрельбы (метод OpenFire) в промежутке между минимальным и максимальным
    //         //Invoke("OpenFire", Random.Range(shotTimeMin, shotTimeMax));
    // }
    
    // private void OpenFire()
    // {
    //     //   Если шанс настал - стреляет
    //     if (Random.value < (float)shotChance / 100)
    //     {
    //         // Создает пулю с позиции врага
    //         Instantiate(obj_Bullet, transform.position, Quaternion.identity);
    //     }
    // }

    public void GetDamage(int damage)
    {

        enemyHealth -= damage;

        if (enemyHealth <= 0)
        {
            Destruction();
            LevelController.instance.countKills += 1;
        }
    }

    private void Destruction()
    {

        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Player")
        {
            GetDamage(1);
            Player.instance.GetDamage(1);
        }
    }
}
