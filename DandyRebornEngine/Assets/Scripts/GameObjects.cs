using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjects : MonoBehaviour
{
    // Скорость движения
    public float speedObject;

    private void Update()
    {
        // Движение фона
        transform.Translate(Vector3.up * speedObject * Time.deltaTime);
    }
}
