using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveByCheckpoints : MonoBehaviour
{
     // Массив координат
    public Transform[] checkPoints;
    public float enemySpeed;
    public bool isReturn;
    [HideInInspector] 
    public Vector3[] _nextPosition;

    private int curPosition;

    private void Start()
    {
            _nextPosition = NextPositioinByChkPts(checkPoints);
            transform.position = _nextPosition[0];
    }
    
    private void Update()
    {
        // Двигаем объект к следующей точки до момента достижения с заданной скоростью
        transform.position = Vector3.MoveTowards(transform.position, _nextPosition[curPosition], enemySpeed * Time.deltaTime);
        // Если добрался, отмечаем что идем к следующей точке массива
        if (Vector3.Distance(transform.position, _nextPosition[curPosition]) < 0.2f)
        {
            //  Инкреминируем номер следующей точки
            curPosition++;
            //   На случай если враг должен вернуться обратно
             if (isReturn && Vector3.Distance(transform.position, _nextPosition[_nextPosition.Length - 1]) < 0.3f)
                 curPosition = 0;
        }
        // В случае достижения финальной точки и условии что враг не возвратный, уничтожается
        if (Vector3.Distance(transform.position, _nextPosition[_nextPosition.Length - 1]) < 0.2f && !isReturn)
        {
            Destroy(gameObject);
        }
    }

    Vector3[] NextPositioinByChkPts(Transform[] pathPos)
    {
        Vector3[] onPositions = new Vector3[pathPos.Length];
        for (int i = 0; i < checkPoints.Length; i++)
        {
            onPositions[i] = pathPos[i].position;
        }
        //Debug.Log("На входе" +  onPositions.Length);
        onPositions = Smoothing(onPositions);
        //Debug.Log("На выходе" +  onPositions.Length);
        return onPositions;
    }

    Vector3[] Smoothing(Vector3[] checkPoints)
    {
        Vector3[] new_checkPoints = new Vector3[(checkPoints.Length - 2) * 2 + 2];
        new_checkPoints[0] = checkPoints[0];
        new_checkPoints[new_checkPoints.Length - 1] = checkPoints[checkPoints.Length - 1];


        int j = 1;
        for (int i = 0; i < checkPoints.Length - 2; i++)
        {
            new_checkPoints[j] = checkPoints[i] + (checkPoints[i + 1] - checkPoints[i]) * 0.75f;
            new_checkPoints[j + 1] = checkPoints[i + 1] + (checkPoints[i + 2] - checkPoints[i + 1]) * 0.25f;
            j += 2;
        }
        return new_checkPoints;
    }
}
