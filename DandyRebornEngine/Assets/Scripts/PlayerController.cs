using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Borders
{
    //  Отступы границ
    public float minX_Offset = 1.1f;
    public float maxX_Offset = 1.1f;
    public float minY_Offset = 1.1f;
    public float maxY_Offset = 1.1f;

    // Значения границ для игрока
    [HideInInspector]
    public float minX, maxX, minY, maxY, topY;
}

[System.Serializable]
public class Guns
{
    public GameObject objCentralGun, objRightGun, objLeftGun;
}

public class PlayerController : MonoBehaviour
{
    
    public static PlayerController instance;
    public Borders borders;
    public int speedPlayer = 5;
    private Camera _camera;
    private Vector2 _mouse_Position;

    //  Оружейный блок
    public Guns guns;
    public GameObject objBullet;
    public float timerReload;
    public float timeBulletSpawn = 0.3f;
    [Range(1, 5)]
    public int curPowerLevelGuns = 1;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        _camera = Camera.main;
    }

    private void Start()
    {
        ResizeBorders();
    }

    private void Update()
    {
        // По нажатию ПКМ
        if (Input.GetMouseButton(1))
        {
            //  Получаем позицию клика
            _mouse_Position = _camera.ScreenToWorldPoint(Input.mousePosition);
           // _mouse_Position.x += 1; 
            // Двигаем игрока на клик
            transform.position = Vector2.MoveTowards(transform.position, _mouse_Position, speedPlayer * Time.deltaTime);
        }
        // В случае если игрок выползает за границы не даем переместиться
        transform.position = new Vector2(Mathf.Clamp(transform.position.x, borders.minX, borders.maxX),
                                         Mathf.Clamp(transform.position.y, borders.minY, borders.maxY));
        // По нажатию ЛКМ
        if (Input.GetMouseButton(0)) {
            if (Time.time > timerReload)
            {
                timerReload = Time.time + timeBulletSpawn;
                MakeAShot();
            }
        }
        
        if (Input.GetKey("escape"))  // если нажата клавиша Esc (Escape)
        {
            Application.Quit();    // закрыть приложение
        }
   
        // //  Изменение параметров игрока от счета
        // if (speedPlayer < 15)
        // {
        //     speedPlayer += LevelController.instance.countKills / 30;
        // }

        // if (timeBulletSpawn > 0.f)
        // {
        //     timeBulletSpawn -= LevelController.instance.countKills / 100;
        // }
    }

    //  Расчет нужен для запуска на разных экранах
    private void ResizeBorders()
    {
        //  !!!!Переписать в пропорции
        borders.minX = _camera.ViewportToWorldPoint(Vector2.zero).x  + borders.minX_Offset;
        borders.minY = _camera.ViewportToWorldPoint(Vector2.zero).y  + borders.minY_Offset;
        borders.maxX = _camera.ViewportToWorldPoint(Vector2.right).x - borders.maxX_Offset;
        borders.maxY = _camera.ViewportToWorldPoint(Vector2.zero).y  + borders.minY_Offset;
        borders.topY = _camera.ViewportToWorldPoint(Vector2.up).y    - borders.maxY_Offset;
    }

    private void CreateBullet(GameObject bullet, Vector3 positionBullet, Vector3 rotationBullet)
    {
         Instantiate(bullet, positionBullet, Quaternion.Euler(rotationBullet));
    }
    private void MakeAShot()
    {
        curPowerLevelGuns = Mathf.RoundToInt(LevelController.instance.countKills / 20 );

        switch (curPowerLevelGuns)
        {
            // Стрельбы из оружия 1 уровня
            case 0:
                CreateBullet(objBullet, guns.objCentralGun.transform.position, Vector3.zero);
                break;
            // Стрельбы из оружия 2 уровня
            case 1:
                CreateBullet(objBullet, guns.objRightGun.transform.position, Vector3.zero);
                CreateBullet(objBullet, guns.objLeftGun.transform.position, Vector3.zero);
                break;
            // Стрельбы из оружия 3 уровня
            case 2:
                CreateBullet(objBullet, guns.objCentralGun.transform.position, Vector3.zero);
                CreateBullet(objBullet, guns.objRightGun.transform.position, new Vector3(0, 0, -5));
                CreateBullet(objBullet, guns.objLeftGun.transform.position, new Vector3(0, 0, 5));
                break;
            // Стрельбы из оружия 4 уровня
            case 3:
                CreateBullet(objBullet, guns.objCentralGun.transform.position, Vector3.zero);
                CreateBullet(objBullet, guns.objRightGun.transform.position, new Vector3(0, 0, 0));
                CreateBullet(objBullet, guns.objRightGun.transform.position, new Vector3(0, 0, -2));
                CreateBullet(objBullet, guns.objLeftGun.transform.position, new Vector3(0, 0, 0));
                CreateBullet(objBullet, guns.objLeftGun.transform.position, new Vector3(0, 0, 2));
                break;
            // Стрельбы из оружия 5 уровня
            case 4:
                CreateBullet(objBullet, guns.objCentralGun.transform.position, Vector3.zero);
                CreateBullet(objBullet, guns.objRightGun.transform.position, new Vector3(0, 0, -5));
                CreateBullet(objBullet, guns.objRightGun.transform.position, new Vector3(0, 0, -15));
                CreateBullet(objBullet, guns.objLeftGun.transform.position, new Vector3(0, 0, 5));
                CreateBullet(objBullet, guns.objLeftGun.transform.position, new Vector3(0, 0, 15));
                break;
            // Стрельбы из оружия сверхуровня уровня  
            default:
                CreateBullet(objBullet, guns.objCentralGun.transform.position, Vector3.zero);
                CreateBullet(objBullet, guns.objRightGun.transform.position, new Vector3(0, 0, -5));
                CreateBullet(objBullet, guns.objRightGun.transform.position, new Vector3(0, 0, -15));
                CreateBullet(objBullet, guns.objLeftGun.transform.position, new Vector3(0, 0, 5));
                CreateBullet(objBullet, guns.objLeftGun.transform.position, new Vector3(0, 0, 15));
                break;

        }
    }
}
