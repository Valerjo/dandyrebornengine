using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    // Размер урона
    public int damage;
    // Флаг игрок или враг
    public bool isEnemyBullet;

    // Метод разрушения объекта  ??? Вынести как публичный???
    private void Destruction()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        // Если пуля вражеская а объект игрок урон игроку.
        if (isEnemyBullet && coll.tag == "Player")
        {
            // Отправляем размер урона в метод получения урона игроком
            Player.instance.GetDamage(damage);
            // Пуля разрушается
            Destruction();
        }
        // Если всё наоборот
        else if (!isEnemyBullet && coll.tag == "Enemy")
        {
            // При столкновении враг получает урон
            coll.GetComponent<Enemies>().GetDamage(damage);
            Destruction();
        }
    }
}
