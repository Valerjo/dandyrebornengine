using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    
    // Ссылка на игрока для доступа из других скриптов
    public static Player instance = null;
    // Объем здоровья
    public int player_Health = 5;

    private void Awake()
    {
        // Если экземпляра нет, создаем, есть уничтожаем.
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    
    // Метод получения урона
    public void GetDamage(int damage)
    {
        // Уменьшаем здоровье на количество урона
        player_Health -= damage;

        // Гибель
        if (player_Health <= 0)
        {
            // Если здоровья нет, вызываем метод уничтожения игрока
            Destruction();
        }
    }
    // Метод разрушения игрока
    void Destruction()
    {
        Destroy(gameObject);
    }
}
