using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaveSettings
{
    //  Шанc стрельбы
    [Range(0, 100)]
    public int shotChance;
    [Range(0, 100)]
    public int attackChance;
    //  Время до выстрела.
    public float shotTimeMin, shotTimeMax;
    //  Время до атаки.
    public float attackTimeMin, attackTimeMax;
    // Скорость движения врага
    public float speedEnemy;
    public bool isReturn;
}

public class Wave : MonoBehaviour
{
    public static Wave instance = null;
    //  Ссылка на настройки атаки волны
    public WaveSettings waveSettings;
    [Space]
    // Массив врагов
    public GameObject[] enemyOfWave;
    //  Массив точек перемещения
    public Transform[] checkPoints;
    private MoveByCheckpoints followComponent;
    private MoveByCheckpoints followAttack;
    private Enemies enemyComponentScript2;
    private Vector2 enemyPositionatWave;
    public float enemyReload;
    public float enemyBulletSpawn = 5;
    private void Awake()
    {
        // Если экземпляра нет, создаем, есть уничтожаем.
        if (instance == null)
        {
            instance = this;
            //  Определяем траектории для этой волны
            followComponent = this.GetComponent<MoveByCheckpoints>();
            //  Определяем чекпоинты из префаба волны
            followComponent.checkPoints = checkPoints;
            // Определяем скорость его движения
            followComponent.enemySpeed = waveSettings.speedEnemy;
            //  Определяем его возвращаемость
            followComponent.isReturn = waveSettings.isReturn;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Start() {

    }
    public void Update()
    {
        //enemyPositionatWave = enemyOfWave[12].transform.position;
        //Debug.Log(enemyPositionatWave.x + "<-x enemy y->" + enemyPositionatWave.y);
        //Debug.Log(transform.position.x + "<-x centerWave  y->" + transform.position.y);
        // Гибель волны
        if (GameObject.FindGameObjectsWithTag("Enemy").Length <= 0)
        {
            Destruction();
        }
    }
    // Метод разрушения волны
    void Destruction()
    {
        Destroy(gameObject);
    }

    void OnDrawGizmos()
    {
        NewPositionByPath(checkPoints);
    }

    void NewPositionByPath(Transform[] path)
    {
        Vector3[] path_Positions = new Vector3[path.Length];
        for (int i = 0; i < path.Length; i++)
        {
            path_Positions[i] = path[i].position;
        }
        path_Positions = Smoothing(path_Positions);
        path_Positions = Smoothing(path_Positions);
        path_Positions = Smoothing(path_Positions);
        for (int i = 0; i < path_Positions.Length - 1; i++)
        {
            Gizmos.DrawLine(path_Positions[i], path_Positions[i + 1]);
        }
    }

    Vector3[] Smoothing(Vector3[] path_Positions)
    {
        Vector3[] new_Path_Positions = new Vector3[(path_Positions.Length - 2) * 2 + 2];
        new_Path_Positions[0] = path_Positions[0];
        new_Path_Positions[new_Path_Positions.Length - 1] = path_Positions[path_Positions.Length - 1];

        int j = 1;
        for (int i = 0; i < path_Positions.Length - 2; i++)
        {
            new_Path_Positions[j] = path_Positions[i] + (path_Positions[i + 1] - path_Positions[i]) * 0.75f;
            new_Path_Positions[j + 1] = path_Positions[i + 1] + (path_Positions[i + 2] - path_Positions[i + 1]) * 0.25f;
            j += 2;
        }
        return new_Path_Positions;
    }
    
}
