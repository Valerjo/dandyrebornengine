using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


[System.Serializable]
public class EnemiesWaves
{
    // Время старта
    //public float timeToStart;
    // Ссылка на префаб волны
    public GameObject wave;
    // Заключительная часть волн
    public bool isLastWave;
}

public class LevelController : MonoBehaviour
{
    // Ссылка на себя  
    public static LevelController instance;
    //  Массив количества кораблей ирока
    public GameObject[] playerShip;
    // Массив вражеских волн
    public EnemiesWaves[] enemyWaves;

    public bool isFinal = false;
    [HideInInspector]
    public int countKills;

    public int i = 0;

    private void Awake()
    {
        //  Настройка ссылки на смого себя, если уже есть уничтожаем
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        // // Создание номерных волн в рагов Depricated
        // for (int i = 0; i < enemyWaves.Length; i++)
        // {
        //     // Создание волн в корутине (время создания и тип волны врагов)
        //     StartCoroutine(CreateEnemyWave(enemyWaves[i].timeToStart, enemyWaves[i].wave, enemyWaves[i].isLastWave));
        // }
        
    }
    
    private void Update()
    {
        //  Проверяем что волн нет и игра не финальная
        if ( isFinal == false && GameObject.FindGameObjectsWithTag("Wave").Length == 0 )
        {
            CreateEnemyWave(enemyWaves[i].wave, enemyWaves[i].isLastWave);
            i++;
        }
        // Если дошли до финала
        if ( isFinal == true && GameObject.FindGameObjectsWithTag("Enemy").Length == 0 )
        {
            // Победа
            Debug.Log("Win");
        }
        if (Player.instance == null)
        {
            // Проигрыш
            Debug.Log("Lose");
        }
    }
    //  Depricated for moved waves
    // IEnumerator CreateEnemyWave(float delay, GameObject Wave, bool finalWave)
    // {
    //     // Настало время создать волну и игрок жив будет создана волна
    //     if (delay != 0) //  Проверка на задержку
    //         yield return new WaitForSeconds(delay);
    //     if (Player.instance != null) //  Живчик?
    //         Instantiate(Wave);

    //     //  Временный ограничитель, окончания игры
    //     if ( finalWave == true) 
    //     {
    //         isFinal = finalWave;
    //     }
    // }

    // Создание Префабной волны
    private void CreateEnemyWave(GameObject Wave, bool finalWave)
    {
        // Настало время создать волну и игрок жив будет создана волна
        if (Player.instance != null) //  Живчик?
            Instantiate(Wave);
        //  Окончания игры
        if ( finalWave == true) 
        {
            isFinal = finalWave;
        }
    }
}