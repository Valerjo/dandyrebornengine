using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShootingSettings
{
    //  Шанc стрельбы
    [Range(0, 100)]
    public int shotChance;
    //  Время до выстрела.
    public float shotTimeMin, shotTimeMax;
}

public class Waves : MonoBehaviour
{
   //  Ссылка на настройки стрельбы

    public ShootingSettings shootingSettings;
    [Space]
    // Префабы врагов
    public GameObject obj_Enemy;
    // Количество вранов в волне
    public int count_in_Wave;
    // Скорость движения врага
    public float speed_Enemy;
    //  Время между респауном
    public float time_Spawn;
    //  Массив точек перемещения
    public Transform[] checkPoints;
    //  Уничтожаются ли враги в конце пути или возвращаются
    public bool is_return;

    //  Тестовая волна
    [Header("Test wave!")]
    public bool is_Test_Wave;

    private MoveByCheckpoints followComponent;
    private Enemies enemyComponentScript;

    private void Start()
    {
        // При старте запускаю корутину по генерации врагов
        StartCoroutine(CreateEnemyWave());
    }

    IEnumerator CreateEnemyWave()
    {
        //  Создание врагов
        for (int i = 0; i < count_in_Wave; i++)
        {
            //  Создаем представителя класса префабов 
            GameObject newEnemy = Instantiate(obj_Enemy, obj_Enemy.transform.position, Quaternion.identity);

            // Получаем от префаба комбонент движения по чекпоинтам.
            followComponent = newEnemy.GetComponent<MoveByCheckpoints>();
            // Определяем траекторию движения нового врага
            followComponent.checkPoints = checkPoints;
            // Определяем скорость его движения
            followComponent.enemySpeed = speed_Enemy;
            //  Определяем его возвращаемость
            followComponent.isReturn = is_return;

            enemyComponentScript = newEnemy.GetComponent<Enemies>();
            enemyComponentScript.shotChance  = shootingSettings.shotChance;
            enemyComponentScript.shotTimeMin = shootingSettings.shotTimeMin;
            enemyComponentScript.shotTimeMax = shootingSettings.shotTimeMax;

            newEnemy.SetActive(true);
            // Задержка
            yield return new WaitForSeconds(time_Spawn);
        }
        // Для тестовой волны
        if (is_Test_Wave)
        {
            //  Генерирует волны каждые 5 секунд
            yield return new WaitForSeconds(5f);
            StartCoroutine(CreateEnemyWave());
        }

        // If is_return = false уничтожаем врага
        if (!is_return) Destroy(gameObject);
    }
    
    
    void OnDrawGizmos()
    {
        NewPositionByPath(checkPoints);
    }

    void NewPositionByPath(Transform[] path)
    {
        Vector3[] path_Positions = new Vector3[path.Length];
        for (int i = 0; i < path.Length; i++)
        {
            path_Positions[i] = path[i].position;
        }
        path_Positions = Smoothing(path_Positions);
        path_Positions = Smoothing(path_Positions);
        path_Positions = Smoothing(path_Positions);
        for (int i = 0; i < path_Positions.Length - 1; i++)
        {
            Gizmos.DrawLine(path_Positions[i], path_Positions[i + 1]);
        }
    }

    Vector3[] Smoothing(Vector3[] path_Positions)
    {
        Vector3[] new_Path_Positions = new Vector3[(path_Positions.Length - 2) * 2 + 2];
        new_Path_Positions[0] = path_Positions[0];
        new_Path_Positions[new_Path_Positions.Length - 1] = path_Positions[path_Positions.Length - 1];

        int j = 1;
        for (int i = 0; i < path_Positions.Length - 2; i++)
        {
            new_Path_Positions[j] = path_Positions[i] + (path_Positions[i + 1] - path_Positions[i]) * 0.75f;
            new_Path_Positions[j + 1] = path_Positions[i + 1] + (path_Positions[i + 2] - path_Positions[i + 1]) * 0.25f;
            j += 2;
        }
        return new_Path_Positions;
    }
    
}
