using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualObjects : MonoBehaviour
{

    public GameObject[] obj_Planets;
    public float time_Planet_Spawn;
    public float speed_Planets;
    List<GameObject> planetsList = new List<GameObject>();

    private void Start()
    {
        StartCoroutine(PlanetsCreation());
    }

    IEnumerator PlanetsCreation()
    {
        // Заполняем лист планет
        for (int i = 0; i < obj_Planets.Length; i++)
        {
            planetsList.Add(obj_Planets[i]);
        }
        // Отсчитываем 7 секунд от старта игры
        yield return new WaitForSeconds(7);
        //  Создаем планеты
        while (true)
        {
            // Выбираем случайный индекс для для создания из листа планет
            int randomIndex = Random.Range(0, planetsList.Count);
            // Создаем случайную планету из листа за пределами видимости камеры  с выстрелом под случайным углом от -25 до +25
            GameObject newPlanet = Instantiate(planetsList[randomIndex],
                new Vector2(Random.Range(PlayerController.instance.borders.minX, PlayerController.instance.borders.maxX),
                PlayerController.instance.borders.topY * 2.2f),
                Quaternion.Euler(0, 0, Random.Range(-25, 25)));

            //  Удаляем использованную планету из листа
            planetsList.RemoveAt(randomIndex);
            // Если лист закончился создаем новый лист планет
            if (planetsList.Count == 0)
            {
                for (int i = 0; i < obj_Planets.Length; i++)
                {
                    planetsList.Add(obj_Planets[i]);
                }
            }
            //  Для созданной планеты задаем скорость движения
            newPlanet.GetComponent<GameObjects>().speedObject = speed_Planets;
            // Делаем задержку перед стартом новой планеты
            yield return new WaitForSeconds(time_Planet_Spawn);
        }
    }
}
